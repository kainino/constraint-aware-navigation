Shader "Global-Mapper" {
    Properties {
        _LowerColor ("LowerColor", Color) = (0.0, 0.0, 1.0, 0.5)
        _LowerLevel ("LowerLevel", Float) = -10
        _InertColor ("InertColor", Color) = (0.0, 0.0, 0.0, 0.5)
        _InertLevel ("InertLevel", Float) = 0
        _UpperColor ("UpperColor", Color) = (0.0, 1.0, 1.0, 0.5)
        _UpperLevel ("UpperLevel", Float) = 10
    }
    SubShader {
        Fog { Mode Off }
        Tags {
            "LightMode" = "Always"
            "Queue" = "Transparent"
            "RenderType" = "Transparent"
        }
        CGPROGRAM
        #pragma surface surf Lambert vertex:vert alpha
        struct Input {
            float3 customColor;
            float3 worldPos;
        };
        void vert (inout appdata_full v, out Input o) {
        }
        float4 _LowerColor;
        float  _LowerLevel;
        float4 _InertColor;
        float  _InertLevel;
        float4 _UpperColor;
        float  _UpperLevel;
        void surf (Input IN, inout SurfaceOutput o) {
            float y = IN.worldPos.y;
            if (y < _LowerLevel) {
                o.Albedo = _LowerColor;
                o.Alpha = _LowerColor.a;
            }
            if (y >= _LowerLevel && y < _InertLevel) {
                float factor = (y - _LowerLevel) / (_InertLevel - _LowerLevel);
                o.Albedo = lerp(_LowerColor  , _InertColor  , factor);
                o.Alpha  = lerp(_LowerColor.a, _InertColor.a, factor);
            }
            if (y >= _InertLevel && y < _UpperLevel) {
                float factor = (y - _InertLevel) / (_UpperLevel - _InertLevel);
                o.Albedo = lerp(_InertColor  , _UpperColor  , factor);
                o.Alpha  = lerp(_InertColor.a, _UpperColor.a, factor);
            }
            if (y >= _UpperLevel) {
                o.Albedo = _UpperColor;
                o.Alpha = _UpperColor.a;
            }
        }
        ENDCG
    }
    Fallback "Transparent/Diffuse"
}