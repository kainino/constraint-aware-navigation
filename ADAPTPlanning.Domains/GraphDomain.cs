namespace ADAPTPlanning.Domains
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// A planner domain implemented using an adjacency list undirected graph.
    /// </summary>
    public class GraphDomain<T>
        : Domain<T>
        where T : State
    {
        /// <summary>
        /// Map from states to collections of adjacent states.
        /// </summary>
        public Dictionary<T, ICollection<T>> Adjs { get; private set; }

        /// <summary>
        /// Constructs a new, empty <see cref="GraphDomain{T}"/>.
        /// </summary>
        public GraphDomain()
        {
            Adjs = new Dictionary<T, ICollection<T>>();
        }

        /// <summary>
        /// Finds the successors 
        /// </summary>
        /// <returns>The successors.</returns>
        /// <param name="s">S.</param>
        public override IEnumerable<Transition<T>> GetSuccessors(T s)
        {
            return Adjs[s].Select(a => new Transition<T>(s, a));
        }

        /// <inheritdoc/>
        public override IEnumerable<Transition<T>> GetPredecessors(T s)
        {
            return GetSuccessors(s)
                .Select(t => new Transition<T>(t.To, t.From));
        }

        /// <inheritdoc/>
        public override T StateForPosition(Vector3 pos)
        {
            float mindist = float.PositiveInfinity;
            T nearest = null;
            foreach (var s in Adjs.Keys) {
                var dist = Vector3.Distance(pos, s.Position);
                if (dist < mindist) {
                    mindist = dist;
                    nearest = s;
                }
            }
            return nearest;
        }
    }
}
