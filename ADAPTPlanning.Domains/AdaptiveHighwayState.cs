namespace ADAPTPlanning.Domains
{
    /// <summary>
    /// A state in the adaptive highway domain.
    /// </summary>
    public class AdaptiveHighwayState
        : GridStateDeferred
    {
        /// <summary>
        /// The number of skips (transitions without branching) remaining in
        /// this direction.
        /// </summary>
        /// <value>The skips left.</value>
        public int SkipsLeft { get; set; }

        /// <summary>
        /// The amount of constraint weight seen so far, used to ensure state
        /// density is high near constraints.
        /// </summary>
        /// <value>The weight so far.</value>
        public float WeightSoFar { get; set; }

        /// <summary>
        /// The current direction in which this highway is traveling.
        /// </summary>
        /// <value>The direction.</value>
        public Vector3 Direction { get; set; }

        /// <summary>
        /// Construct a state in the adaptive highway domain.
        /// </summary>
        public AdaptiveHighwayState(int x, int y, int z)
            : base(x, y, z)
        {
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            var ahs = obj as AdaptiveHighwayState;
            if (ahs == null) {
                return false;
            }
            return base.Equals(obj);// && Direction == ahs.Direction;
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return base.GetHashCode();
            //Vector3 dir = Direction * System.Math.Sign(Direction.X);
            //int x = (int) dir.X;
            //int y = (int) dir.Y;
            //int z = (int) dir.Z;
            //int hash = (x << 24) | (y << 16) | (z << 8);
            //return hash ^ base.GetHashCode();
        }
    }
}

