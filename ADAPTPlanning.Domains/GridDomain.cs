namespace ADAPTPlanning.Domains
{
    using System.Collections.Generic;

    /// <summary>
    /// An adjacency-list based grid domain.
    /// </summary>
    public class GridDomain
        : GraphDomain<State>
    {
        readonly State[, ] nodes;

        /// <summary>
        /// The array of nodes in the grid.
        /// </summary>
        /// <value>The nodes.</value>
        public State[, ] Nodes {
            get { return nodes; }
        }

        /// <summary>
        /// Construct a new <see cref="GridDomain"/>.
        /// </summary>
        /// <param name="sizex"> X size of the grid domain.</param>
        /// <param name="sizey"> Y size of the grid domain.</param>
        public GridDomain(int sizex, int sizey)
        {
            nodes = new State[sizex, sizey];

            for (int x = 0; x < sizex; x++) {
                for (int y = 0; y < sizey; y++) {
                    nodes[x, y] = new State { Position = new Vector3(x, 0, y) };
                }
            }

            for (int x = 0; x < sizex; x++) {
                for (int y = 0; y < sizey; y++) {
                    var c = nodes[x, y];
                    for (int dx = -1; dx <= 1; dx++) {
                        var x1 = x + dx;
                        if (x1 < 0 || x1 >= sizex) {
                            continue;
                        }
                        for (int dy = -1; dy <= 1; dy++) {
                            var y1 = y + dy;
                            if (y1 < 0 || y1 >= sizey) {
                                continue;
                            }
                            AddEdge(c, nodes[x1, y1]);
                        }
                    }
                }
            }
        }

        void AddEdge(State s, State state)
        {
            var adjlist = Adjs.GetValueOrInsertDefault(
                s, _ => new HashSet<State>());
            adjlist.Add(state);
        }
    }
}
