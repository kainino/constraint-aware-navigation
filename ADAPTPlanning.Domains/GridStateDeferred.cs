namespace ADAPTPlanning.Domains
{
    /// <summary>
    /// A state in the deferred grid domain.
    /// </summary>
    public class GridStateDeferred
        : State
    {
        /// <summary> Grid x coordinate. </summary>
        protected readonly int X;
        /// <summary> Grid y coordinate. </summary>
        protected readonly int Y;
        /// <summary> Grid z coordinate. </summary>
        protected readonly int Z;

        /// <summary>
        /// Create a new deferred grid state.
        /// </summary>
        public GridStateDeferred(int x, int y, int z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            var gsd = obj as GridStateDeferred;
            if (gsd == null) {
                return false;
            }
            return X == gsd.X && Y == gsd.Y && Z == gsd.Z;
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return (X << 20) ^ (Y << 10) ^ Z;
        }
    }
}
