namespace ADAPTPlanning.Domains
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// A deferred grid domain, which generates a grid domain on the fly.
    /// </summary>
    public class GridDomainDeferred
        : Domain<State>
    {
        /// <summary>
        /// Spacing between the grid states.
        /// </summary>
        public float Spacing { get; private set; }

        /// <summary>
        /// Construct a new deferred grid domain with the given grid spacing.
        /// </summary>
        public GridDomainDeferred(float spacing)
        {
            Spacing = spacing;
        }

        /// <inheritdoc/>
        public override IEnumerable<Transition<State>> GetSuccessors(State s)
        {
            var ret = new Transition<State>[8];
            int i = 0;

            var p = s.Position;
            for (int dx = -1; dx <= 1; dx++) {
                for (int dz = -1; dz <= 1; dz++) {
                    if (dx == 0 && dz == 0) {
                        continue;
                    }
                    var topos = new Vector3(p.X + dx * Spacing, 0, p.Z + dz * Spacing);
                    var to = StateForPosition(topos);
                    ret[i] = new Transition<State>(s, to);
                    i++;
                }
            }

            return ret;
        }

        /// <inheritdoc/>
        public override IEnumerable<Transition<State>> GetPredecessors(State s)
        {
            return GetSuccessors(s)
                .Select(t => new Transition<State>(t.To, t.From));
        }

        /// <inheritdoc/>
        public override State StateForPosition(Vector3 pos)
        {
            int x = (int) Math.Floor(pos.X / Spacing + 0.5);
            int y = (int) Math.Floor(pos.Y / Spacing + 0.5);
            int z = (int) Math.Floor(pos.Z / Spacing + 0.5);
            var p = new Vector3(x * Spacing, y * Spacing, z * Spacing);
            return new GridStateDeferred(x, y, z) { Position = p };
        }
    }
}
