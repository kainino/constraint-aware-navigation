namespace ADAPTPlanning.Domains
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using AHS = AdaptiveHighwayState;

    /// <summary>
    /// Domain using adaptive highways--reducing the branching factor to improve
    /// performance at the cost of environment representation accuracy.
    /// </summary>
    public class AdaptiveHighwayDomain
        : Domain<AHS>
    {
        /// <summary>
        /// Grid spacing between domain states.
        /// </summary>
        public float Spacing { get; private set; }

        /// <summary>
        /// The planner used with the domain.
        /// </summary>
        public Planner<Domain<AHS>, AHS> Planner { get; set; }

        /// <summary>
        /// Construct a new adaptive highway domain instance.
        /// </summary>
        /// <param name="spacing">State grid spacing.</param>
        public AdaptiveHighwayDomain(float spacing)
        {
            Spacing = spacing;
        }

        /// <summary>
        /// Decide how many steps to take, based on the multiplier field.
        /// </summary>
        public override IEnumerable<Transition<AHS>> GetSuccessors(AHS s)
        {
            const float maxstepdist = 4;
            float maxstep = maxstepdist / Spacing;

            var p = s.Position;
            var pwt = Planner.Weighting.WeightField(p);
            var pnext = p + s.Direction;
            var pnextwt = Planner.Weighting.WeightField(pnext);
            var plast = p - s.Direction;

            if (float.IsInfinity(pwt) || float.IsInfinity(pnextwt)) {
                return new Transition<AHS>[0];
            }

            // Check whether we should continue on this skip-path. If there is
            // too much weight in this area, or we've reached the maximum skip
            // depth, we'll go on to make 8 transitions instead of 1.
            if (s.SkipsLeft > 0 && s.WeightSoFar < maxstepdist) {
                var ret1 = new Transition<AHS>[2];
                var to = StateForPosition(pnext);
                to.SkipsLeft = s.SkipsLeft - 1;
                to.Direction = s.Direction;
                if (Vector3.Distance(p, Planner.SearchGoal.Position) < maxstep) {
                    to.WeightSoFar = float.PositiveInfinity;
                } else {
                    float wsf = s.WeightSoFar + Math.Abs(pwt);
                    to.WeightSoFar = wsf;
                }
                ret1[0] = new Transition<AHS>(s, to);
                ret1[1] = new Transition<AHS>(s, StateForPosition(plast));
                return ret1;
            }

            // Done skipping or skipping was terminated; go in any direction.
            var ret = new Transition<AHS>[8];
            int i = 0;

            for (int dx = -1; dx <= 1; dx++) {
                for (int dz = -1; dz <= 1; dz++) {
                    if (dx == 0 && dz == 0) {
                        continue;
                    }
                    var dir = new Vector3(dx * Spacing, 0, dz * Spacing);
                    var to = StateForPosition(p + dir);
                    // Maximum skip distance, often will really be shorter
                    to.SkipsLeft = (int) maxstep;//StepField(p, dir);
                    to.Direction = dir;
                    to.WeightSoFar = Math.Abs(pwt);
                    ret[i] = new Transition<AHS>(s, to);
                    i++;
                }
            }

            return ret;
        }

        /// <inheritdoc/>
        public override IEnumerable<Transition<AHS>> GetPredecessors(AHS s)
        {
            return GetSuccessors(s).Select(t => new Transition<AHS>(t.To, t.From));
        }

        /// <inheritdoc/>
        public override AHS StateForPosition(Vector3 pos)
        {
            int x = (int) Math.Floor(pos.X / Spacing + 0.5);
            int y = (int) Math.Floor(pos.Y / Spacing + 0.5);
            int z = (int) Math.Floor(pos.Z / Spacing + 0.5);
            var p = new Vector3(x * Spacing, y * Spacing, z * Spacing);
            return new AHS(x, y, z) {
                SkipsLeft = 0, WeightSoFar = 0, Position = p
            };
        }
    }
}
