namespace ADAPTPlanning.Planners
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ADAPTPlanning.Planners.Structures;

    /// <summary>
    /// An A* path planner.
    /// </summary>
    public partial class AStar<T>
        : Planner<Domain<T>, T>
        where T : State
    {
        /// <summary>
        /// The heap of open search nodes.
        /// </summary>
        protected BinaryMinHeap<Node> OpenHeap;

        /// <summary>
        /// A lookup map of states to search nodes.
        /// </summary>
        protected Dictionary<T, Node> Nodes;

        float reachedH;
        T reachedState;

        /// <inheritdoc/>
        /// <summary>
        /// Constructs a new <see cref="AStar{T}"/> planner.
        /// </summary>
        public AStar(Domain<T> domain)
            : base(domain)
        {
        }

        /// <inheritdoc/>
        public override int NodesVisited {
            get { return Nodes == null ? 0 : Nodes.Count; }
        }

        /// <inheritdoc/>
        public override T Reached {
            get { return reachedState; }
        }

        /// <inheritdoc/>
        public override IEnumerable<Transition<T>> Visited {
            get {
                return Nodes == null ? null :
                    Nodes.Values.Select(n => n.Transition);
            }
        }

        /// <summary>
        /// Gets the visited nodes.
        /// </summary>
        /// <value>The visited nodes.</value>
        public IEnumerable<Node> VisitedNodes {
            get { return Nodes == null ? null : Nodes.Values; }
        }

        /// <inheritdoc/>
        public override IEnumerable<T> Open {
            get {
                return OpenHeap == null ? null :
                    OpenHeap.Contents.Select(n => n.State);
            }
        }

        /// <inheritdoc/>
        protected override IEnumerable<Transition<T>> GetPlan {
            get { return Status.PathAvailable ? PlanReversed().Reverse() : null; }
        }

        IEnumerable<Transition<T>> PlanReversed()
        {
            var tr = Nodes[Reached].Transition;
            while (tr != null) {
                yield return tr;
                tr = Nodes[tr.From].Transition;
            }
            yield break;
        }

        /// <inheritdoc/>
        protected override void DoInitialize()
        {
            var startNode = new Node(Start) {
                PastCost = 0f,
                CostEstimate = Estimate(Start, Goal) };
            OpenHeap = new BinaryMinHeap<Node>();
            OpenHeap.Insert(startNode);
            Nodes = new Dictionary<T, Node>();
            Nodes.Add(Start, startNode);
            reachedState = null;
            reachedH = float.PositiveInfinity;
        }

        /// <inheritdoc/>
        protected override Status DoStep()
        {
            if (OpenHeap.Count == 0) {
                return Status.FinishedNoPath;
            }

            var u = OpenHeap.Pop();
            if (u.CostEstimate - u.PastCost < reachedH) {
                reachedH = u.CostEstimate - u.PastCost;
                reachedState = u.State;
            }
            u.Closed = true;
            if (u.State == Goal) {
                return Status.FinishedOptimal;
            }
            // If it's not accessible, then nothing is.
            if (float.IsInfinity(u.CostEstimate)) {
                return Status.FinishedNoPath;
            }

            foreach (var tr in Domain.GetSuccessors(u.State)) {
                var newg = u.PastCost + Integrate(tr);
                var v = tr.To;

                var vnode = Nodes.GetValueOrInsertDefault(v, k => {
                    // Make a new node and insert it into both open and nodes.
                    var n = new Node(k);
                    OpenHeap.Insert(n);
                    return n;
                });

                if (newg < vnode.PastCost) {
                    vnode.Transition = tr;
                    vnode.PastCost = newg;
                    vnode.CostEstimate = newg + Estimate(v, Goal);
                    UpdateIncons(vnode);
                }
            }

            return Reached == null ? Status.Running : Status.RunningPartial;
        }

        /// <summary>
        /// Called when the inconsistent states need to be handled.
        /// </summary>
        /// <param name="n">N.</param>
        protected virtual void UpdateIncons(Node n)
        {
            OpenHeap.HeapUp(n);
        }

        /// <summary>
        /// Estimates the cost between from one state to another.
        /// </summary>
        protected virtual float Estimate(T from, T to)
        {
            return Domain.ComputeHeuristicEstimate(from, to);
        }
    }
}
