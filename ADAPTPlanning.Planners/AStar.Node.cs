namespace ADAPTPlanning.Planners
{
    using System;

    public partial class AStar<T>
    {
        /// <summary>
        /// A search node for <see cref="AStar{T}"/>
        /// </summary>
        public class Node
            : IComparable<Node>
        {
            Transition<T> transition;

            /// <summary>
            /// State at this search node.
            /// </summary>
            public T State { get; private set; }

            /// <summary>
            /// Transition used to get to this state.
            /// </summary>
            public Transition<T> Transition {
                get { return transition; }
                set {
                    if (value.To != State) {
                        throw new ArgumentException(
                            "New transition does not end at this state.");
                    }
                    transition = value;
                }
            }

            /// <summary>
            /// The cost of the path up until this state.
            /// </summary>
            public float PastCost { get; set; }

            /// <summary>
            /// The estimated total cost from start to finish.
            /// </summary>
            public float CostEstimate { get; set; }

            /// <summary>
            /// Whether this node is closed.
            /// </summary>
            public bool Closed { get; set; }

            /// <summary>
            /// Constructs a new <see cref="Node"/> at the given state.
            /// </summary>
            public Node(T value)
            {
                State = value;
                PastCost = float.PositiveInfinity;
                CostEstimate = float.PositiveInfinity;
                Closed = false;
            }

            /// <inheritdoc/>
            public int CompareTo(Node other)
            {
                // If they are very close, we can pretend they are equal, which
                // improves the performance by causing the heap to behave LIFO.
                // We don't worry about the fact that we are doing an absolute-
                // epsilon floating point comparison, because it's okay if this
                // test fails (only making the performance slightly worse).
                if (Math.Abs(CostEstimate - other.CostEstimate) < 0.000001f) {
                    return 0;
                }
                return CostEstimate.CompareTo(other.CostEstimate);
            }
        }
    }
}
