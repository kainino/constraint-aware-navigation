namespace ADAPTPlanning.Planners
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// The ARA* planner.
    /// </summary>
    public class ARAStar<T>
        : AStar<T>
        where T : State
    {
        IEnumerable<Transition<T>> publishedPlan;
        bool deflated;
        readonly float initialInflation;

        /// <summary>
        /// Current inflaction factor.
        /// </summary>
        public float Inflation { get; private set; }

        /// <summary>
        /// Step by which to decrease inflation factor.
        /// </summary>
        public float InflationStep { get; private set; }

        /// <summary>
        /// Set of inconsistent search nodes.
        /// </summary>
        public HashSet<Node> Incons { get; private set; }

        /// <summary>
        /// Construct a new ARA* planner instance.
        /// </summary>
        /// <param name="domain">Planner domain.</param>
        /// <param name="initInfl">Initial inflation factor.</param>
        /// <param name="inflStep">Inflation decreate step size.</param>
        public ARAStar(
            Domain<T> domain, float initInfl, float inflStep)
            : base(domain)
        {
            initialInflation = initInfl;
            InflationStep = inflStep;
        }

        /// <summary>
        /// The inconsistent nodes.
        /// </summary>
        public IEnumerable<Node> InconsNodes {
            get { return Incons; }
        }

        /// <inheritdoc/>
        protected override IEnumerable<Transition<T>> GetPlan {
            get { return publishedPlan ?? base.GetPlan;}
        }

        /// <inheritdoc/>
        protected override void DoInitialize()
        {
            publishedPlan = null;
            deflated = false;
            Inflation = initialInflation;
            Incons = new HashSet<Node>();
            base.DoInitialize();
        }

        /// <inheritdoc/>
        protected override Status DoStep()
        {
            Status st = base.DoStep();

            Node ngoal;
            bool improvePathStopped = Nodes.TryGetValue(Goal, out ngoal) &&
                ngoal.CostEstimate <= OpenHeap.Min.CostEstimate &&
                st.PathReachesGoal;
            if (improvePathStopped) {
                publishedPlan = base.GetPlan.ToArray();
                if (Inflation > 1f) {
                    DecreaseEpsilon();
                    deflated = true;
                } else {
                    return Status.FinishedOptimal;
                }
            }

            if (deflated) {
                return Status.RunningSuboptimal;
            }

            return st;
        }

        /// <inheritdoc/>
        protected override void UpdateIncons(Node n)
        {
            if (n.Closed) {
                OpenHeap.Remove(n);
                Incons.Add(n);
            } else {
                OpenHeap.HeapUp(n);
            }
        }

        /// <inheritdoc/>
        protected override float Estimate(T from, T to)
        {
            return base.Estimate(from, to) * Inflation;
        }

        void DecreaseEpsilon()
        {
            // Decrease epsilon
            Inflation -= InflationStep;
            if (Inflation < 1.001f) {
                Inflation = 1f;
            }
            // Move states from INCONS to OPEN
            foreach (var i in Incons) {
                OpenHeap.Insert(i);
            }
            Incons.Clear();
            var goalnode = Nodes[Goal];
            if (!OpenHeap.Contains(goalnode)) {
                OpenHeap.Insert(goalnode);
            }
            // Update the priorities for all s in OPEN
            foreach (var n in OpenHeap.Contents) {
                n.CostEstimate = n.PastCost + Estimate(n.State, Goal);
            }
            OpenHeap.Heapify();
            // CLOSED = empty
            foreach (var n in Nodes.Values) {
                n.Closed = false;
            }
        }
    }
}
