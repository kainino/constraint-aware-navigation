namespace ADAPTPlanning.Planners
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ADAPTPlanning.Planners.Structures;

    /// <summary>
    /// A path planner using Dijkstra's algorithm.
    /// </summary>
    public partial class Dijkstra<T>
        : Planner<Domain<T>, T>
        where T : State
    {
        BinaryMinHeap<Node> open;
        Dictionary<T, Node> nodes;
        T reached;
        IEnumerable<Transition<T>> plan;

        /// <summary>
        /// The set of closed search nodes.
        /// </summary>
        public HashSet<Node> Closed { get; private set;}

        /// <inheritdoc/>
        /// <summary>
        /// Construct a new <see cref="Dijkstra{T}"/> planner.
        /// </summary>
        public Dijkstra(Domain<T> domain)
            : base(domain)
        {
        }

        /// <inheritdoc/>
        public override int NodesVisited {
            get { return nodes == null ? 0 : nodes.Count; }
        }

        /// <inheritdoc/>
        public override T Reached {
            get { return Status.PathAvailable ? reached : null; }
        }

        /// <summary>
        /// Whether this instance of the planner can ever reach suboptimality.
        /// </summary>
        /// <value><c>true</c> if suboptimable; otherwise, <c>false</c>.</value>
        protected virtual bool Suboptimable {
            get { return false; }
        }

        /// <summary>
        /// The set of visited search nodes.
        /// </summary>
        public IEnumerable<Node> VisitedNodes {
            get { return nodes == null ? null : nodes.Values; }
        }

        /// <inheritdoc/>
        public override IEnumerable<Transition<T>> Visited {
            get {
                return nodes == null ? null :
                    nodes.Values.Select(n => n.Transition);
            }
        }

        /// <inheritdoc/>
        public override IEnumerable<T> Open {
            get {
                return open == null ? null :
                    open.Contents.Select(n => n.State);
            }
        }

        /// <summary>
        /// The heap of open search nodes.
        /// </summary>
        protected BinaryMinHeap<Node> OpenHeap {
            get { return open; }
        }

        /// <inheritdoc/>
        protected override IEnumerable<Transition<T>> GetPlan {
            get {
                return Status.PathAvailable ? plan : null;
            }
        }

        IEnumerable<Transition<T>> PlanReversed()
        {
            var tr = nodes[Reached].Transition;
            while (tr != null) {
                yield return tr;
                tr = nodes[tr.From].Transition;
            }
            yield break;
        }

        /// <inheritdoc/>
        protected override void DoInitialize()
        {
            var startnode = new Node(Start) {
                CostEstimate = ComputeInitialCost(Start) };
            open = new BinaryMinHeap<Node>();
            open.Insert(startnode);
            nodes = new Dictionary<T, Node>();
            nodes.Add(Start, startnode);
            Closed = new HashSet<Node>();
            reached = null;
            plan = PlanReversed().Reverse();
        }

        /// <inheritdoc/>
        protected override Status DoStep()
        {
            if (open.Count == 0) {
                return Status.FinishedNoPath;
            }

            // Pop the most promising node from the heap.
            var u = open.Pop();
            // We know that it is optimized.
            Closed.Add(u);
            // So this is the best node we've found so far.
            reached = u.State;
            // If it's the goal, we're done.
            if (u.State == Goal) {
                return Status.FinishedOptimal;
            }
            // If it's not accessible, then nothing is.
            if (float.IsPositiveInfinity(u.CostEstimate)) {
                return Status.FinishedNoPath;
            }

            foreach (var tr in Domain.GetSuccessors(u.State)) {
                var v = tr.To;
                var vnode = nodes.GetValueOrInsertDefault(v, k => {
                    // Make a new node and insert it into both open and nodes.
                    var n = new Node(k);
                    open.Insert(n);
                    return n;
                });
                // If this node is already optimized, we're done with it.
                if (Closed.Contains(vnode)) {
                    continue;
                }
                // If not, try to optimize it further.
                var alt = u.CostEstimate + ComputeReducedCost( tr);
                if (alt < vnode.CostEstimate) {
                    vnode.CostEstimate = alt;
                    vnode.Transition = tr;
                    open.HeapUp(vnode);
                }
            }

            return (reached != null && Suboptimable) ?
                Status.RunningPartial : Status.Running;
        }

        /// <summary>
        /// Computes the initial cost estimate of a start state.
        /// </summary>
        protected virtual float ComputeInitialCost(T start)
        {
            return 0f;
        }

        /// <summary>
        /// Get the reduced cost of a transition. This can be overridden by
        /// planners with modified reduced costs (such as A*-consistent).
        /// </summary>
        protected virtual float ComputeReducedCost(Transition<T> tr)
        {
            return Integrate(tr);
        }
    }
}
