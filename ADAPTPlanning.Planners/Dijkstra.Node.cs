namespace ADAPTPlanning.Planners
{
    using System;

    public partial class Dijkstra<T>
    {
        /// <summary>
        /// A search node in a Dijkstra's algorithm search.
        /// </summary>
        public class Node
            : IComparable<Node>
        {
            Transition<T> transition;

            /// <summary>
            /// The state at this node.
            /// </summary>
            public T State { get; private set; }

            /// <summary>
            /// The transition reaching this node.
            /// </summary>
            public Transition<T> Transition {
                get { return transition; }
                set {
                    if (value.To != State) {
                        throw new ArgumentException(
                            "New transition does not end at this state.");
                    }
                    transition = value;
                }
            }

            /// <summary>
            /// The current cost estimate for this search node.
            /// </summary>
            /// <value>The cost estimate.</value>
            public float CostEstimate { get; set; }

            /// <summary>
            /// Constructs a new search node with a given start state
            /// </summary>
            public Node(T state)
            {
                State = state;
                CostEstimate = float.PositiveInfinity;
            }

            /// <inheritdoc/>
            public int CompareTo(Node other)
            {
                // If they are very close, we can pretend they are equal, which
                // improves the performance by causing the heap to behave LIFO.
                // We don't worry about the fact that we are doing an absolute-
                // epsilon floating point comparison, because it's okay if this
                // test fails (only making the performance slightly worse).
                if (Math.Abs(CostEstimate - other.CostEstimate) < 0.000001f) {
                    return 0;
                }
                return CostEstimate.CompareTo(other.CostEstimate);
            }

            /// <inheritdoc/>
            public override string ToString()
            {
                return string.Format("[Node: State={0}, Transition={1}, CostEstimate={2}]", State, Transition, CostEstimate);
            }
        }
    }
}
