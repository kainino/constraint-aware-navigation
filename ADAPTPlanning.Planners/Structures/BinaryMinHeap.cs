using System.Collections.Generic;
using System;

namespace ADAPTPlanning.Planners.Structures
{
    /// <summary>
    /// Binary min-heap, used as priority queue.
    /// </summary>
    public class BinaryMinHeap<T>
        where T : class, IComparable<T>
    {
        readonly Dictionary<T, int> reverse = new Dictionary<T, int>();
        readonly List<T> heap = new List<T>();

        /// <summary>
        /// The contents of the heap.
        /// </summary>
        public IEnumerable<T> Contents {
            get { return heap; }
        }

        /// <summary>
        /// The minimum element in the heap.
        /// </summary>
        public T Min {
            get {
                if (heap.Count == 0) {
                    throw new InvalidOperationException(
                        "The heap is empty.");
                }
                return heap[0];
            }
        }

        /// <summary>
        /// Pop the minimum element from the heap.
        /// </summary>
        public T Pop()
        {
            T min = Min;
            Remove(0);
            return min;
        }

        /// <summary>
        /// Insert a value into the heap.
        /// </summary>
        public void Insert(T node)
        {
            //if (reverse.ContainsKey(node)) {
            //    throw new ArgumentException(
            //        "This element already exists in the heap.");
            //}
            heap.Add(node);
            reverse.Add(node, heap.Count - 1);
            HeapUp(heap.Count - 1);
        }

        /// <summary>
        /// True if the heap contains a value.
        /// </summary>
        public bool Contains(T node)
        {
            return reverse.ContainsKey(node);
        }

        /// <summary>
        /// Number of elements in the heap.
        /// </summary>
        public int Count {
            get { return heap.Count; }
        }

        /// <summary>
        /// Remove an element from the heap by value.
        /// </summary>
        public bool Remove(T node)
        {
            int i;
            if (reverse.TryGetValue(node, out i)) {
                Remove(i);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Perform the heap-up operation on a node.
        /// </summary>
        /// <returns>False if no such element.</returns>
        public bool HeapUp(T node)
        {
            int i;
            if (reverse.TryGetValue(node, out i)) {
                HeapUp(i);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Performs the heap-down operation on a node.
        /// </summary>
        /// <returns>False if no such element.</returns>
        public bool HeapDown(T node)
        {
            int i;
            if (reverse.TryGetValue(node, out i)) {
                HeapDown(i);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Heapify the heap.
        /// </summary>
        public void Heapify()
        {
            for (int i = Parent(heap.Count - 1); i >= 0; i--) {
                HeapDown(i);
            }
        }

        void Remove(int index)
        {
            int last = heap.Count - 1;
            SwapNodes(index, last);
            reverse.Remove(heap[last]);
            heap.RemoveAt(last);
            HeapDown(index);
        }

        void SwapNodes(int i, int j)
        {
            T temp = heap[i];
            heap[i] = heap[j];
            heap[j] = temp;
            reverse[heap[i]] = i;
            reverse[heap[j]] = j;
        }

        int HeapUp(int i)
        {
            T node = heap[i];
            while (true) {
                int parent = Parent(i);
                // Use <, not <=, so that it heaps up through equal elements.
                if (parent < 0 || heap[parent].CompareTo(node) < 0) {
                    break;
                }
                SwapNodes(i, parent);
                i = parent;
            }
            return i;
        }

        int HeapDown(int i)
        {
            if (i >= heap.Count) {
                return i;
            }
            while (true) {
                int l = Left(i);
                int r = Right(i);

                if (l > heap.Count - 1 || r > heap.Count - 1) {
                    break;
                }
                int child = heap[l].CompareTo(heap[r]) < 0 ? l : r;
                // Use >, not >=, so that it heaps down through equal elements.
                if (heap[child].CompareTo(heap[i]) > 0) {
                    break;
                }
                SwapNodes(i, child);
                i = child;
            }
            return i;
        }

        static int Parent(int i) {
            int par = (i + 1) / 2 - 1;
            return i == 0 ? -1 : par;
        }

        static int Left(int i) {
            return 2 * i + 1;
        }

        static int Right(int i) {
            return 2 * i + 2;
        }
    }
}
 