namespace ADAPTPlanning.Planners
{
    /// <summary>
    /// An A* path planner based on <see cref="Dijkstra{T}"/>, optimal in
    /// monotone (consistent) heuristics only.
    /// </summary>
    public class AStarMonotone<T>
        : Dijkstra<T>
        where T : State
    {
        /// <inheritdoc/>
        /// <summary>
        /// Construct a new <see cref="AStarMonotone{T}"/> planner.
        /// </summary>
        public AStarMonotone(Domain<T> domain)
            : base(domain)
        {
        }

        /// <inheritdoc/>
        protected override float ComputeInitialCost(T start)
        {
            return Estimate(start, Goal);
        }

        /// <inheritdoc/>
        protected override float ComputeReducedCost(Transition<T> tr)
        {
            return Integrate(tr)
                - Estimate(tr.From, Goal)
                + Estimate(tr.To, Goal);
        }

        /// <summary>
        /// Compute the heuristic estmate between two states.
        /// </summary>
        protected virtual float Estimate(T from, T to)
        {
            return Domain.ComputeHeuristicEstimate(from, to);
        }

        /// <inheritdoc/>
        protected override bool Suboptimable {
            get { return true; }
        }
    }
}
