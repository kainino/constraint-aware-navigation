namespace ADAPTPlanning.Planners
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// A breadth first search "planner." Ignores transition costs.
    /// Doesn't really plan anything, but provides a basic test case.
    /// </summary>
    public class BFS<T>
        : Planner<Domain<T>, T>
        where T : State
    {
        Queue<T> open;
        Dictionary<T, Transition<T>> transitions;
        T reached;

        /// <inheritdoc/>
        /// <summary>
        /// Constructs a new BFS planner.
        /// </summary>
        public BFS(Domain<T> domain)
            : base(domain)
        {
        }

        /// <inheritdoc/>
        public override int NodesVisited {
            get { return transitions == null ? 0 : transitions.Count; }
        }

        /// <inheritdoc/>
        public override T Reached {
            get { return reached; }
        }

        /// <inheritdoc/>
        public override IEnumerable<Transition<T>> Visited {
            get { return transitions == null ? null : transitions.Values; }
        }

        /// <inheritdoc/>
        public override IEnumerable<T> Open {
            get { return open; }
        }

        /// <inheritdoc/>
        protected override IEnumerable<Transition<T>> GetPlan {
            get { return PlanReversed().Reverse(); }
        }

        /// <summary>
        /// Reconstructs a reversed version of the plan.
        /// </summary>
        IEnumerable<Transition<T>> PlanReversed()
        {
            if (!Status.PathAvailable) {
                throw new System.InvalidOperationException("No plan computed!");
            }
            var tr = transitions[Reached];
            while (tr != null) {
                yield return tr;
                tr = transitions[tr.From];
            }
            yield break;
        }

        /// <returns>The reversed.</returns>
        protected override void DoInitialize()
        {
            open = new Queue<T>();
            transitions = new Dictionary<T, Transition<T>>();
            open.Enqueue(Start);
            transitions.Add(Start, null);
            reached = null;
        }

        /// <returns>The reversed.</returns>
        protected override Status DoStep()
        {
            if (open.Count == 0) {
                return Status.FinishedNoPath;
            }

            var t = open.Dequeue();
            if (t.Equals(Goal)) {
                reached = t;
                return Status.FinishedOptimal;
            }

            var successors = Domain.GetSuccessors(t)
                .Where(e => !float.IsPositiveInfinity(Integrate(e)));
            foreach (var e in successors) {
                var u = e.To;
                if (!transitions.ContainsKey(u)) {
                    transitions.Add(u, e);
                    open.Enqueue(u);
                }
            }
            return Status.Running;
        }
    }
}
