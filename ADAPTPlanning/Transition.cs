namespace ADAPTPlanning
{
    /// <summary>
    /// A state transition within the search (e.g. a graph edge).
    /// </summary>
    /// <typeparam name="T">
    /// <see cref="State"/> type used by the planner. </typeparam>
    public class Transition<T>
        where T : State
    {
        /// <summary>
        /// The end point of the transition.
        /// </summary>
        public T To { get; private set; }

        /// <summary>
        /// The beginning point of the transition.
        /// </summary>
        public T From { get; private set; }

        /// <summary>
        /// Construct a new transition between two states.
        /// </summary>
        public Transition(T from, T to)
        {
            From = from;
            To = to;
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return string.Format("[Transition: {0} -> {1}]", To, From);
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return (To.GetHashCode() << 16) ^ From.GetHashCode();
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            return Equals(obj as Transition<T>);
        }

        /// <summary>
        /// Compares equality of two <see cref="Transition{T}"/>s.
        /// </summary>
        public bool Equals(Transition<T> other)
        {
            return To == other.To && From == other.From;
        }
    }
}
