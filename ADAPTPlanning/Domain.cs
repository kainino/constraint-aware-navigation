namespace ADAPTPlanning
{
    using System.Collections.Generic;

    /// <summary>
    /// A search domain for a path planner.
    /// </summary>
    /// <typeparam name="T"> <see cref="State"/> type used by the planner.
    /// </typeparam>
    public abstract class Domain<T>
        where T : State
    {
        /// <summary>
        /// Gets the successor state transitions of a state (i.e. transitions
        /// away from the given state).
        /// </summary>
        public abstract IEnumerable<Transition<T>> GetSuccessors(T s);

        /// <summary>
        /// Gets the predecessor state transitions of a state (i.e. transitions
        /// toward the given state).
        /// </summary>
        /// <returns>The predecessors.</returns>
        /// <param name="s">S.</param>
        public abstract IEnumerable<Transition<T>> GetPredecessors(T s);

        /// <summary>
        /// Returns the state for a given position (e.g. for finding start and
        /// goal states given positions).
        /// </summary>
        public abstract T StateForPosition(Vector3 pos);

        /// <summary>
        /// Computes the base cost to transition between two states.
        /// </summary>
        public virtual float ComputeCost(T from, T to)
        {
            return Vector3.Distance(from.Position, to.Position);
        }

        /// <summary>
        /// Compute the heuristic estimate cost from a state to the goal.
        /// </summary>
        public virtual float ComputeHeuristicEstimate(T state, T goal)
        {
            return ComputeCost(state, goal);
        }
    }
}
