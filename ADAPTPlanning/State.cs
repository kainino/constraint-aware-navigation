namespace ADAPTPlanning
{
    using System;

    /// <summary>
    /// A generic state within the planner search domain.
    /// </summary>
    public class State
    {
        /// <summary>
        /// The spacial position of the state.
        /// </summary>
        public Vector3 Position { get; set; }

        /// <summary>
        /// Returns a string representation.
        /// </summary>
        public override string ToString()
        {
            return string.Format("[State: Position={0}]", Position);
        }

        /// <summary>
        /// Implicit conversion to <see cref="Vector3"/> position.
        /// </summary>
        public static implicit operator Vector3(State state)
        {
            return state.Position;
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            return Equals(obj as State);
        }

        /// <summary>
        /// Tests equality with another <see cref="State"/>.
        /// </summary>
        public bool Equals(State other)
        {
            if (other == null) {
                return false;
            }
            return Position.Equals(other.Position);
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return Position.GetHashCode();
        }

        /// <summary>
        /// Tests equality of two <see cref="State"/>s.
        /// </summary>
        public static bool operator==(State a, State b)
        {
            return Object.ReferenceEquals(a, null) ? 
                Object.ReferenceEquals(b, null) : a.Equals(b);
        }
        
        /// <summary>
        /// Tests inequality of two <see cref="State"/>s.
        /// </summary>
        public static bool operator!=(State a, State b)
        {
            return !(a == b);
        }
    }
}
