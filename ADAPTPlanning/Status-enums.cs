namespace ADAPTPlanning
{
    public partial struct Status
    {
        /// <summary>
        /// The current status of the planner's execution.
        /// </summary>
        public enum RunStatus
        {
            /// <summary>
            /// The planner is uninitialized.
            /// </summary>
            Uninitialized,
            /// <summary>
            /// The planner is still running.
            /// </summary>
            Running,
            /// <summary>
            /// The planner is finished.
            /// </summary>
            Finished,
        }

        /// <summary>
        /// The current status of the planner's output path.
        /// </summary>
        public enum PathStatus
        {
            /// <summary>
            /// No path is available.
            /// </summary>
            Unavailable,
            /// <summary>
            /// A partial path is available.
            /// </summary>
            Partial,
            /// <summary>
            /// A complete suboptimal path is available.
            /// </summary>
            Suboptimal,
            /// <summary>
            /// A complete optimal path is available.
            /// </summary>
            Optimal,
            /// <summary>
            /// This is used as a "desired status" to make sure the planner
            /// never stops unless it does the maximum number of steps.
            /// </summary>
            RunForever,
        }
    }
}
