namespace ADAPTPlanning
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// General-purpose extensions.
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Finds the minimum value in an <see cref="IEnumerable{T}"/>, using
        /// a function as the ordering criterion.
        /// </summary>
        /// <typeparam name="T"> The enumerated type. </typeparam>
        /// <typeparam name="TMin"> The type on which to sort. </typeparam>
        public static T MinBy<T, TMin>(this IEnumerable<T> t, Func<T, TMin> f)
        {
            return t.OrderBy(f).First();
        }

        /// <summary>
        /// Attempts to get a value from a dictionary. Calls a default value
        /// provider function if that fails and adds it to the dictionary.
        /// </summary>
        /// <returns> The appropriate value now in the dictionary. </returns>
        public static TValue GetValueOrInsertDefault<TKey, TValue>(
            this IDictionary<TKey, TValue> dictionary,
            TKey key,
            Func<TKey, TValue> defaultValueProvider)
        {
            TValue value;
            if (!dictionary.TryGetValue(key, out value)) {
                value = defaultValueProvider(key);
                dictionary[key] = value;
            }
            return value;
        }
    }
}
