namespace ADAPTPlanning
{
    using System.Collections.Generic;

    /// <inheritdoc/>
    /// <summary>
    /// A dynamic planner, which handles changes in transition costs.
    /// </summary>
    public abstract class PlannerDynamic<TDomain, T>
        : Planner<TDomain, T>
        where TDomain : Domain<T>
        where T : State
    {
        /// <summary>
        /// True if the start state has changed but not handled.
        /// </summary>
        protected bool StartChanged;

        /// <summary>
        /// All states which have been invalidated but not handled.
        /// </summary>
        protected HashSet<T> Invalid;

        /// <summary>
        /// Returns invalid states, for debugging.
        /// </summary>
        public ICollection<T> InvalidStates {
            get { return Invalid; }
        }

        /// <summary>
        /// The desired start state for the resulting plan.
        /// </summary>
        /// <remarks>This is NOT NECESSARILY the start state of the SEARCH.</remarks>
        /// <value>The start.</value>
        public override T Start {
            get { return base.Start; }
            set {
                base.Start = value;
                StartChanged = true;
            }
        }

        /// <inheritdoc/>
        /// <summary>
        /// Abstract constructor for a dynamic planner.
        /// </summary>
        protected PlannerDynamic(TDomain domain)
            : base(domain)
        {
        }

        /// <inheritdoc/>
        protected override void DoInitialize()
        {
            StartChanged = false;
            Invalid = new HashSet<T>();
        }

        /// <summary>
        /// Mark a set of states as invalid.
        /// </summary>
        public void InvalidateStates(IEnumerable<T> states)
        {
            Invalid.UnionWith(states);
        }
    }
}
