namespace ADAPTPlanning
{
    /// <summary>
    /// Represents the status of a planner execution.
    /// </summary>
    public partial struct Status
    {
        /// <summary>
        /// The planner has not yet been initialized.
        /// </summary>
        public static Status Uninitialized = new Status(
            RunStatus.Uninitialized, PathStatus.Unavailable);

        /// <summary>
        /// The planner is still running, and no path is available.
        /// </summary>
        public static Status Running = new Status(
            RunStatus.Running, PathStatus.Unavailable);

        /// <summary>
        /// The planner is still running and can provide an approximate
        /// partial path.
        /// </summary>
        public static Status RunningPartial = new Status(
            RunStatus.Running, PathStatus.Partial);

        /// <summary>
        /// The planner is still running and can provide a complete
        /// suboptimal path.
        /// </summary>
        public static Status RunningSuboptimal = new Status(
            RunStatus.Running, PathStatus.Suboptimal);

        /// <summary>
        /// The planner is still running and can provide a complete
        /// optimal path.
        /// </summary>
        public static Status RunningOptimal = new Status(
            RunStatus.Running, PathStatus.Optimal);

        /// <summary>
        /// The planner has finished but was unable to find a path.
        /// </summary>
        public static Status FinishedNoPath = new Status(
            RunStatus.Finished, PathStatus.Unavailable);

        /// <summary>
        /// The planner has finished and has found an optimal path.
        /// </summary>
        public static Status FinishedOptimal = new Status(
            RunStatus.Finished, PathStatus.Optimal);

        /// <summary>
        /// The current status of the planner's execution.
        /// </summary>
        public readonly RunStatus Run;

        /// <summary>
        /// The current status of the planner's output path.
        /// </summary>
        public readonly PathStatus Path;

        /// <summary>
        /// True if the planner can provide a path plan.
        /// </summary>
        public bool PathAvailable {
            get { return Path != PathStatus.Unavailable; }
        }

        /// <summary>
        /// True if the planner can provide a complete path plan.
        /// </summary>
        public bool PathReachesGoal {
            get { return PathAvailable && Path != PathStatus.Partial; }
        }

        /// <summary>
        /// Construct a new <see cref="Status"/>.
        /// </summary>
        /// <param name="run"> <see cref="RunStatus"/> of a planner. </param>
        /// <param name="path"> <see cref="PathStatus"/> of a planner. </param>
        Status(RunStatus run, PathStatus path)
        {
            Run = run;
            Path = path;
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return string.Format("[Status: Run={0}, Path={1}]", Run, Path);
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            return Equals((Status) obj);
        }

        /// <summary>
        /// Tests equality with another <see cref="Status"/>.
        /// </summary>
        public bool Equals(Status st)
        {
            return Path == st.Path && Run == st.Run;
        }

        /// <inheritdoc/>
        public static bool operator==(Status s, Status t)
        {
            return s.Equals(t);
        }

        /// <inheritdoc/>
        public static bool operator!=(Status s, Status t)
        {
            return !s.Equals(t);
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            unchecked {
                return (Run.GetHashCode() << 16) & Path.GetHashCode();
            }
        }
    }
}
