namespace ADAPTPlanning
{
    using System;

    /// <summary>
    /// Represents a 3-float vector.
    /// </summary>
    /// <remarks>
    /// Used instead of Unity's Vector3 so that this library doesn't depend
    /// on Unity binaries.
    /// </remarks>
    public struct Vector3
    {
        /// <summary>
        /// X position.
        /// </summary>
        public float X { get; private set; }

        /// <summary>
        /// Y position.
        /// </summary>
        public float Y { get; private set; }

        /// <summary>
        /// Z position.
        /// </summary>
        public float Z { get; private set; }

        /// <summary>
        /// Constructs a new <see cref="Vector3"/> with the given position.
        /// </summary>
        public Vector3(float x, float y, float z)
            : this()
        {
            X = x;
            Y = y;
            Z = z;
        }

        /// <summary>
        /// Euclidean length of the vector.
        /// </summary>
        public double Length()
        {
            return X * X + Y * Y + Z * Z;
        }

        /// <summary>
        /// Computes the squared distance between two <see cref="Vector3"/>s.
        /// </summary>
        static double DistanceSquared(Vector3 a, Vector3 b)
        {
            double
                dx = b.X - a.X,
                dy = b.Y - a.Y,
                dz = b.Z - a.Z;
            return dx * dx + dy * dy + dz * dz;
        }

        /// <summary>
        /// Computes the distance between two <see cref="Vector3"/>s.
        /// </summary>
        public static float Distance(Vector3 a, Vector3 b)
        {
            return (float) Math.Sqrt(DistanceSquared(a, b));
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return string.Format("({0}, {1}, {2})", X, Y, Z);
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            return obj is Vector3 && Equals((Vector3) obj);
        }

        /// <summary>
        /// Compares equality with another <see cref="Vector3"/>.
        /// </summary>
        public bool Equals(Vector3 other)
        {
            return DistanceSquared(this, other) < 0.00001;
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return (X.GetHashCode() << 20) ^
                (Y.GetHashCode() << 10) ^
                Z.GetHashCode();
        }

        /// <inheritdoc/>
        public static bool operator==(Vector3 a, Vector3 b)
        {
            return a.Equals(b);
        }

        /// <inheritdoc/>
        public static bool operator!=(Vector3 a, Vector3 b)
        {
            return !(a == b);
        }

        /// <inheritdoc/>
        public static Vector3 operator+(Vector3 a, Vector3 b)
        {
            return new Vector3(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
        }

        /// <inheritdoc/>
        public static Vector3 operator-(Vector3 a, Vector3 b)
        {
            return new Vector3(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
        }

        /// <inheritdoc/>
        public static Vector3 operator-(Vector3 a)
        {
            return new Vector3(-a.X, -a.Y, -a.Z);
        }

        /// <inheritdoc/>
        public static Vector3 operator*(Vector3 a, float b)
        {
            return new Vector3(a.X * b, a.Y * b, a.Z * b);
        }

        /// <inheritdoc/>
        public static Vector3 operator/(Vector3 a, float b)
        {
            return a * (1f / b);
        }

        /// <inheritdoc/>
        public static Vector3 operator*(float a, Vector3 b)
        {
            return b * a;
        }
    }
}
