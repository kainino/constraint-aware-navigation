namespace ADAPTPlanning
{
    using System;

    /// <summary>
    /// Represents a scalar-valued space-weighting in the planning space.
    /// </summary>
    [Serializable]
    public class Weighting
    {
        /// <summary>
        /// Used to indicate whether a point in space is obstructed.
        /// </summary>
        [NonSerialized]
        public Func<Vector3, bool> Obstructed = _ => false;

        /// <summary>
        /// The base weight (a constant value).
        /// </summary>
        public virtual float WeightBase()
        {
            return 0;
        }

        /// <summary>
        /// Computes the weight field at a given point in space, without base.
        /// (That is, positive = attracting, negative = repelling).
        /// </summary>
        public virtual float WeightField(Vector3 mp)
        {
            return 0;
        }

        /// <summary>
        /// From the base weight and weight field, compute the transition
        /// cost multiplier.
        /// </summary>
        public float CostMultiplier(Vector3 mp)
        {
            if (Obstructed(mp)) {
                return float.PositiveInfinity;
            }

            float mult = (float) Math.Pow(1.1, WeightBase() - WeightField(mp));
            // TODO?: fix non-optimality here
            return mult; //Math.Max(1f, mult); //Mathf.Max(1f, ret);
        }
    }
}

