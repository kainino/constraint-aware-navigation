namespace ADAPTPlanning.Planners
{
    using System;
    using System.Linq;
    using NUnit.Framework;
    using ADAPTPlanning.Domains;

    [TestFixture]
    class Tests
    {
        const long maxTimeMS = 1000000;
        const int Size = 20;
        const int X1 = 4;
        const int Y1 = 10;
        const int X2 = 12;
        const int Y2 = 4;
        static readonly int dX = Math.Abs(X2 - X1);
        static readonly int dY = Math.Abs(Y2 - Y1);
        static readonly int minR = new[] {
            X1, Y1, X2, Y2,
            Size - X1, Size - Y1, Size - X2, Size - Y2 }.Min();
        static readonly int expectedPlanCount = Math.Max(dX, dY);

        GridDomain dom;

        [TestFixtureSetUp]
        public void SetUp()
        {
            dom = new GridDomain(Size, Size);
        }

        [Test]
        public void TestBFS()
        {
            // BFS expands in a square.
            int radius = Math.Max(dX, dY);
            int maxexpansion = 4 * radius * radius;
            int minexpansion = minR * minR;

            var planner = new BFS<State>(dom) {
                Start = dom.Nodes[X1, Y1],
                Goal = dom.Nodes[X2, Y2],
            };

            planner.Compute(Status.PathStatus.Optimal, maxexpansion, maxTimeMS);
            Assert.AreEqual(Status.FinishedOptimal, planner.Status);
            Assert.AreEqual(expectedPlanCount, planner.Plan.Count());
            {
                var visited = planner.NodesVisited;
                Assert.IsTrue(visited <= maxexpansion);
                Assert.IsTrue(
                    minexpansion > maxexpansion || visited >= minexpansion);
            }

            planner.Reset();
            planner.Compute(Status.PathStatus.Optimal, minexpansion, maxTimeMS);
            Assert.AreEqual(Status.Running, planner.Status);
            planner.Compute(Status.PathStatus.Optimal, maxexpansion, maxTimeMS);
            Assert.AreEqual(Status.FinishedOptimal, planner.Status);
            Assert.AreEqual(expectedPlanCount, planner.Plan.Count());
            {
                var visited = planner.NodesVisited;
                Assert.IsTrue(visited <= maxexpansion);
                Assert.IsTrue(
                    minexpansion > maxexpansion || visited >= minexpansion);
            }
        }

        [Test]
        public void TestDijkstra()
        {
            // Dijkstra's algorithm expands in a circle.
            int radiussquared = dX * dX + dY * dY;
            int maxexpansion = (int) (Math.PI * radiussquared);
            int minexpansion = (int) (Math.PI * minR * minR);

            var planner = new Dijkstra<State>(dom) {
                Start = dom.Nodes[X1, Y1],
                Goal = dom.Nodes[X2, Y2],
            };

            planner.Compute(Status.PathStatus.Optimal, maxexpansion, maxTimeMS);
            Assert.AreEqual(Status.FinishedOptimal, planner.Status);
            Assert.AreEqual(expectedPlanCount, planner.Plan.Count());
            {
                var visited = planner.NodesVisited;
                Assert.IsTrue(visited <= maxexpansion);
                Assert.IsTrue(
                    minexpansion > maxexpansion || visited >= minexpansion);
            }

            planner.Reset();
            planner.Compute(Status.PathStatus.Optimal, minexpansion, maxTimeMS);
            Assert.AreEqual(Status.Running, planner.Status);
            planner.Compute(Status.PathStatus.Optimal, maxexpansion, maxTimeMS);
            Assert.AreEqual(Status.FinishedOptimal, planner.Status);
            Assert.AreEqual(expectedPlanCount, planner.Plan.Count());
            {
                var visited = planner.NodesVisited;
                Assert.IsTrue(visited <= maxexpansion);
                Assert.IsTrue(
                    minexpansion > maxexpansion || visited > minexpansion);
            }
        }

        [Test]
        public void TestAStarMonotone()
        {
            // A* expands in a straight line in this environment.
            int maxexpansion = (dX + 2) * (dY + 2);
            int minexpansion = expectedPlanCount;

            var planner = new AStarMonotone<State>(dom) {
                Start = dom.Nodes[X1, Y1],
                Goal = dom.Nodes[X2, Y2],
            };

            planner.Compute(Status.PathStatus.Optimal, maxexpansion, maxTimeMS);
            Assert.AreEqual(Status.FinishedOptimal, planner.Status);
            Assert.AreEqual(expectedPlanCount, planner.Plan.Count());
            {
                var visited = planner.NodesVisited;
                Assert.IsTrue(visited <= maxexpansion);
                Assert.IsTrue(
                    minexpansion > maxexpansion || visited >= minexpansion);
            }

            planner.Reset();
            planner.Compute(Status.PathStatus.Optimal, minexpansion, maxTimeMS);
            Assert.AreEqual(Status.RunningPartial, planner.Status);
            planner.Compute(Status.PathStatus.Optimal, maxexpansion, maxTimeMS);
            Assert.AreEqual(Status.FinishedOptimal, planner.Status);
            Assert.AreEqual(expectedPlanCount, planner.Plan.Count());
            {
                var visited = planner.NodesVisited;
                Assert.IsTrue(visited <= maxexpansion);
                Assert.IsTrue(
                    minexpansion > maxexpansion || visited >= minexpansion);
            }
        }

        [Test]
        public void TestAStar()
        {
            // A* expands in a straight line in this environment.
            int maxexpansion = (dX + 2) * (dY + 2);
            int minexpansion = expectedPlanCount;

            var planner = new AStar<State>(dom) {
                Start = dom.Nodes[X1, Y1],
                Goal = dom.Nodes[X2, Y2],
            };

            planner.Compute(Status.PathStatus.Optimal, maxexpansion, maxTimeMS);
            Assert.AreEqual(Status.FinishedOptimal, planner.Status);
            Assert.AreEqual(expectedPlanCount, planner.Plan.Count());
            {
                var visited = planner.NodesVisited;
                Assert.IsTrue(visited <= maxexpansion);
                Assert.IsTrue(
                    minexpansion > maxexpansion || visited >= minexpansion);
            }

            planner.Reset();
            planner.Compute(Status.PathStatus.Optimal, minexpansion, maxTimeMS);
            Assert.AreEqual(Status.RunningPartial, planner.Status);
            planner.Compute(Status.PathStatus.Optimal, maxexpansion, maxTimeMS);
            Assert.AreEqual(Status.FinishedOptimal, planner.Status);
            Assert.AreEqual(expectedPlanCount, planner.Plan.Count());
            {
                var visited = planner.NodesVisited;
                Assert.IsTrue(visited <= maxexpansion);
                Assert.IsTrue(
                    minexpansion > maxexpansion || visited >= minexpansion);
            }
        }

        [Test]
        public void TestARAStar()
        {
            // ARA* expands in a straight line in this environment.
            int maxexpansion = (dX + 2) * (dY + 2);
            int minexpansion = expectedPlanCount;

            var planner = new ARAStar<State>(dom, 2.5f, 1f) {
                Start = dom.Nodes[X1, Y1],
                Goal = dom.Nodes[X2, Y2],
            };

            planner.Compute(Status.PathStatus.Optimal, maxexpansion, maxTimeMS);
            Assert.AreEqual(Status.FinishedOptimal, planner.Status);
            Assert.AreEqual(expectedPlanCount, planner.Plan.Count());
            {
                var visited = planner.NodesVisited;
                Assert.IsTrue(visited <= maxexpansion);
                Assert.IsTrue(
                    minexpansion > maxexpansion || visited >= minexpansion);
            }

            planner.Reset();
            planner.Compute(Status.PathStatus.Optimal, minexpansion, maxTimeMS);
            Assert.AreEqual(Status.RunningPartial, planner.Status);
            planner.Compute(Status.PathStatus.Optimal, maxexpansion, maxTimeMS);
            Assert.AreEqual(Status.FinishedOptimal, planner.Status);
            Assert.AreEqual(expectedPlanCount, planner.Plan.Count());
            {
                var visited = planner.NodesVisited;
                Assert.IsTrue(visited <= maxexpansion);
                Assert.IsTrue(
                    minexpansion > maxexpansion || visited >= minexpansion);
            }
        }

        [Test]
        public void TestADStarStatic()
        {
            // AD* expands in a straight line in this environment.
            int maxexpansion = (dX + 2) * (dY + 2);
            int minexpansion = expectedPlanCount;

            var planner = new ADStar<State>(dom, 2.5f, 1f) {
                Start = dom.Nodes[X1, Y1],
                Goal = dom.Nodes[X2, Y2],
            };

            planner.Compute(Status.PathStatus.Optimal, maxexpansion, maxTimeMS);
            Assert.AreEqual(Status.RunningOptimal, planner.Status);
            Assert.AreEqual(expectedPlanCount, planner.Plan.Count());
            {
                var visited = planner.NodesVisited;
                Assert.IsTrue(visited <= maxexpansion);
                Assert.IsTrue(
                    minexpansion > maxexpansion || visited >= minexpansion);
            }

            planner.Reset();
            planner.Compute(Status.PathStatus.Optimal, minexpansion, maxTimeMS);
            Assert.AreEqual(Status.RunningPartial, planner.Status);
            planner.Compute(Status.PathStatus.Optimal, maxexpansion, maxTimeMS);
            Assert.AreEqual(Status.RunningOptimal, planner.Status);
            Assert.AreEqual(expectedPlanCount, planner.Plan.Count());
            {
                var visited = planner.NodesVisited;
                Assert.IsTrue(visited <= maxexpansion);
                Assert.IsTrue(
                    minexpansion > maxexpansion || visited >= minexpansion);
            }
        }
    }
}
