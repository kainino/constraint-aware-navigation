# Constraint-Aware Navigation & Planner Framework

A planner framework with extensible and modular planners, domains, states, etc.
as well as an integrated "multiplier"/"area weighting" functionality used for
constraint-aware navigation, as described in "Constraint-Aware Navigation in
Dynamic Environments" [1] and "Planning Approaches to Constraint-Aware
Navigation in Dynamic Environments" [2].

The project in this repository is not the original code used in the papers, and
does not use A. Shoulson's ADAPT framework for animation. However, this
project is rewritten from scratch with readability and good documentation in
mind, so it should prove far more useful for study.


## Running the demo

The provided demo requires at least Unity 5.5, and was most recently tested on
Unity 5.5 (free version).

To use the demo on Windows, build the MonoDevelop solution (`.sln` file) in
Debug mode, then open the Unity project `UnityDebugger` (which loads the
built-and-copied DLLs). (WARNING: Don't open the Unity Project until after you
have built the project once; otherwise it will break the script references!)

Depending on your Unity version, you may need to build using an external copy
of MonoDevelop. On non-Windows, the copy command will fail, so you will
need to copy the build DLL into the Unity project before opening it.

If the broken script references cause the Unity project to break permanently,
use Git to re-checkout the files in the Unity project which have changed.

Once the Unity project is open, load one of the scenes from the `Assets/Scenes`
directory. The main demo is `HumanConstraint.unity`.

In order to see the plan visualization, **BE SURE** to enable Gizmos in the
Unity player (using the Gizmos button in the top right of the "Game" panel in
the Unity editor). A standalone build will **NOT** be able to show the plan
visualization.


## Known Issues

https://bitbucket.org/kainino/constraint-aware-navigation/issues?status=new&status=open


## References

[1] Kapadia, M., Ninomiya, K., Shoulson, A., Garcia, F., and Badler, N.I. "Constraint-Aware Navigation in Dynamic Environments." ACM SIGGRAPH Conference on Motion in Games (MIG), 2013. [[preprint](http://www.stwing.upenn.edu/~kainino/ConstraintAwareNavigation.pdf)]

[2] Ninomiya, K., Kapadia, M., Shoulson, A., Garcia, F., and Badler, N. “Planning Approaches to Constraint-Aware Navigation in Dynamic Environments.” Computer Animation and Virtual Worlds, 26: 119–139, 2015. [[preprint](http://www.stwing.upenn.edu/~kainino/PlanningApproaches.pdf)] [doi: [10.1002/cav.1622](http://dx.doi.org/10.1002/cav.1622)]


## License

All work in this repository is, where appropriate, Copyright 2013 of the
Authors of [1]/[2] and the University of Pennsylvania. All rights reserved.

Contact:

* Kai Ninomiya <kainino1, gmail>
* Mubbasir Kapadia <mubbasir.kapadia, gmail>
