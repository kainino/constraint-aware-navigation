namespace ADAPTPlanning.UnityTest.Planners
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ADAPTPlanning.Planners;

    /// <inheritdoc/>
    /// <summary>
    /// Automatically-invalidating version of <see cref="ADStar{T}"/>. Scans
    /// the graph transitions for changed costs.
    /// </summary>
    public class ADStarAuto<T>
        : ADStar<T>
        where T : State
    {
        Dictionary<Transition<T>, float> mulState;

        /// <summary>
        /// Constructs a new <see cref="ADStarAuto{T}"/> planner.
        /// </summary>
        /// <param name="domain"> Search domain. </param>
        /// <param name="initInfl"> Initial inflation factor. </param>
        /// <param name="inflStep"> Inflation factor reduction step. </param>
        public ADStarAuto(Domain<T> domain, float initInfl, float inflStep)
            : base(domain, initInfl, inflStep)
        {
        }

        /// <summary>
        /// Check for any changes in the planning problem (due to constraint or obstacle changes)
        /// and invalidate appropriately.
        /// </summary>
        public void DetectChanges() {
            InvalidateAutomaticBackward(Visited);
        }

        IEnumerable<Transition<T>> Changed()
        {
            return mulState.Where(
                kvp => Math.Abs(Integrate(kvp.Key) - kvp.Value)
                    > 0.0001f)
                .Select(kvp => kvp.Key);
        }

        /// <summary>
        /// Automatically invalidate the given transitions
        /// (for a backward searching planner).
        /// </summary>
        protected void InvalidateAutomaticBackward(
            IEnumerable<Transition<T>> trs)
        {
            if (mulState != null) {
                InvalidateStates(Changed().Select(t => t.From));
            }
            SaveMultiplierState(trs);
        }

        /// <summary>
        /// Automatically invalidate the given transitions
        /// (for a forward searching planner).
        /// </summary>
        protected void InvalidateAutomaticForward(
            IEnumerable<Transition<T>> trs)
        {
            if (mulState != null) {
                var invalid = Changed().Select(t => t.To);
                if (invalid.Any()) {
                    InvalidateStates(invalid);
                }
            }
            SaveMultiplierState(trs);
        }

        /// <summary>
        /// Saves the state of the multiplier field at the given transitions.
        /// </summary>
        void SaveMultiplierState(IEnumerable<Transition<T>> trs)
        {
            if (mulState == null) {
                mulState = new Dictionary<Transition<T>, float>();
            }
            mulState.Clear();
            foreach (var tr in trs) {
                mulState[tr] = Integrate(tr);
            }
        }
    }
}
