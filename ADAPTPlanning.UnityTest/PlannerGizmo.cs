namespace ADAPTPlanning.UnityTest
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using UnityEngine;
    using ADAPTPlanning.ConstraintAware;
    using ADAPTPlanning.Domains;
    using ADAPTPlanning.Planners;
    using ADAPTPlanning.UnityTest.Planners;

    #if ADAPTIVE
    using TS = ADAPTPlanning.Domains.AdaptiveHighwayState;
    #else
    using TS = State;
    #endif

    /// <summary>
    /// This MonoBehavior allows for testing and debugging the planner inside
    /// Unity, using gizmos.
    /// </summary>
    public partial class PlannerGizmo : MonoBehaviour
    {
        StreamWriter file;
        /// <summary>
        /// Start state.
        /// </summary>
        protected TS S1;
        /// <summary>
        /// Goal state.
        /// </summary>
        protected TS S2;

        IEnumerable<BoxCollider> obstacles;

        /// <summary>
        /// Domain used by the planner.
        /// </summary>
        public Domain<TS> Dom;

        string plannerText = "";
        string statusText = "";
        /// <summary>
        /// Whether to show the planner control buttons.
        /// </summary>
        public bool ShowButtons = true;

        /// <summary>
        /// The planner used to plan.
        /// </summary>
        protected virtual Planner<Domain<TS>, TS> Planner { get; set; }

        /// <summary>
        /// Behavior specification to use with the planner.
        /// </summary>
        public ConstraintSet Behavior;

        /// <summary>
        /// The parent GameObject containing all of the obstacle BoxColliders.
        /// </summary>
        public GameObject ObstaclesParent;

        /// <summary>
        /// Start position transform.
        /// </summary>
        public Transform StartPos;

        /// <summary>
        /// Goal position transform.
        /// </summary>
        public Transform GoalPos;

        /// <summary>
        /// The distance between adjacent states in the deferred grid domain.
        /// </summary>
        public float GridSep = 1f;

        /// <summary>
        /// Maximum number of steps per execution of the planner.
        /// </summary>
        public int MaxSteps = 1000;

        /// <summary>
        /// Maximum time to iterate during each execution of the planner.
        /// </summary>
        public long MaxTimeMS = 16;

        /// <summary>
        /// Mesh used to display the weight field.
        /// </summary>
        public MeshFilter wtField;

        /// <summary>
        /// Set up the obstacles, domain, and start/goal states.
        /// </summary>
        public virtual void Start()
        {
            obstacles = ObstaclesParent.GetComponentsInChildren<BoxCollider>();
            //gen = new GraphGeneratorGrid(NavBound, GridSep, NeighborRadius);
            //Dom = gen.Dom;
            #if ADAPTIVE
            Dom = new AdaptiveHighwayDomain(GridSep);
            #else
            Dom = new GridDomainDeferred(GridSep);
            #endif
            S1 = Dom.StateForPosition(StartPos.position.FromUnity());
            S2 = Dom.StateForPosition(GoalPos.position.FromUnity());
            file = new StreamWriter("planner" + this.GetHashCode() +  ".csv");
        }

        /// <summary>
        /// Clean up when the gizmo is destroyed.
        /// </summary>
        public void OnDestroy()
        {
            file.Close();
        }

        int framenum = 0;
        float plantime = 0;

        /// <summary>
        /// Handle keypresses to (1) set up the planner and (2) step/execute
        /// the planner.
        /// </summary>
        public virtual void Update()
        {
            var oldplanner = Planner;
            if (Input.GetKeyDown(KeyCode.Alpha1)) {
                Planner = new BFS<TS>(Dom);
            } else if (Input.GetKeyDown(KeyCode.Alpha2)) {
                Planner = new Dijkstra<TS>(Dom);
            } else if (Input.GetKeyDown(KeyCode.Alpha3)) {
                Planner = new AStarMonotone<TS>(Dom);
            } else if (Input.GetKeyDown(KeyCode.Alpha4)) {
                Planner = new AStar<TS>(Dom);
            } else if (Input.GetKeyDown(KeyCode.Alpha5)) {
                Planner = new ARAStar<TS>(Dom, 2.5f, 1f);
            } else if (Input.GetKeyDown(KeyCode.Alpha6)) {
                Planner = new ADStar<TS>(Dom, 2.5f, 1f);
            } else if (Input.GetKeyDown(KeyCode.Alpha7)) {
                Planner = new ADStarAuto<TS>(Dom, 2.5f, 1f);
            }

            if (Planner != oldplanner) {
                plannerText = string.Format("Planner: {0}",
                    Planner.GetType().Name);
                Debug.Log(plannerText);
                Planner.Start = S1;
                Planner.Goal = S2;
                Behavior.Obstructed = v => obstacles.Any(o => o.Contains(v));
                Planner.Weighting = Behavior;
                framenum = 0;
                plantime = 0;
                file.WriteLine("frame,planTime,inflation,planCostEst");
                #if ADAPTIVE
                var d = Dom as AdaptiveHighwayDomain;
                if (d != null) {
                    d.Planner = Planner;
                }
                #endif
            }

            if (Planner != null) {
                bool updatestatus = false;

                if (Input.GetKey(KeyCode.C)) {
                    var pl = Planner as ADStarAuto<TS>;
                    if (pl != null) {
                        Debug.Log("Auto-detecting environment changes");
                        pl.DetectChanges();
                    }
                }

                if (Input.GetKey(KeyCode.A)) {
                    var t1 = Time.realtimeSinceStartup;
                    Planner.Compute(Status.PathStatus.Optimal, MaxSteps, MaxTimeMS);
                    var t2 = Time.realtimeSinceStartup;
                    plantime += t2 - t1;
                    var pl = Planner as ADStar<TS>;
                    if (pl != null) {
                        file.WriteLine("{0},{1},{2},{3}",
                            framenum,
                            plantime,
                            pl.Inflation,
                            pl.PlanEstimatedCost
                        );
                    }
                    framenum += 1;
                    updatestatus = true;
                } else if (Input.GetKeyDown(KeyCode.S)) {
                    Planner.Step();
                    updatestatus = true;
                } else if (Input.GetKey(KeyCode.D)) {
                    var t1 = Time.realtimeSinceStartup;
                    Planner.Step();
                    var t2 = Time.realtimeSinceStartup;
                    plantime += t2 - t1;
                    var pl = Planner as ADStar<TS>;
                    if (pl != null) {
                        file.WriteLine("{0},{1},{2},{3}",
                            framenum,
                            plantime,
                            pl.Inflation,
                            pl.PlanEstimatedCost
                        );
                    }
                    updatestatus = true;
                } else if (Input.GetKeyDown(KeyCode.R)) {
                    Planner.Reset();
                    updatestatus = true;
                }
                if (updatestatus) {
                    statusText = string.Format("{0}, visited={1}, w={2}", 
                        Planner.Status,
                        Planner.NodesVisited,
                        Planner.Reached == null ? "N/A" :
                        Planner.Weighting.CostMultiplier(Planner.Reached.Position).ToString());
                    Debug.Log(statusText);
                }
            }

            VisualizeField();
        }

        /// <summary>
        /// Update the weight visualization field.
        /// </summary>
        public void VisualizeField()
        {
            if (wtField == null) {
                return;
            }
            var t = wtField.transform;
            var m = wtField.mesh;
            var vs = m.vertices;
            // calculate mult field - KN
            for (int i = 0; i < vs.Length; i++) {
                var v = t.TransformPoint(vs[i]);
                var p = new ADAPTPlanning.Vector3(v.x, t.position.y, v.z);
                //vs[i].y = 4 - Behavior.Multiplier(p);
                vs[i].y = Behavior.WeightField(p) / 4 + 2.2f;
            }
            m.vertices = vs;
        }

        /// <summary>
        /// Draws buttons on the screen when a planner is active.
        /// </summary>
        void OnGUI() {
            if (ShowButtons && Planner != null) {
                bool updatestatus = false;
                GUI.Label(new Rect(Screen.width - 200, 0, 200, 20), plannerText);
                GUI.Label(new Rect(Screen.width - 400, 20, 400, 20), statusText);
                if (GUI.Button(new Rect(Screen.width - 150, 40, 150, 20),
                    "Compute to Optimal")) {
                    Planner.Compute(Status.PathStatus.Optimal, MaxSteps, MaxTimeMS);
                    updatestatus = true;
                }
                if (GUI.Button(new Rect(Screen.width - 150, 60, 150, 20),
                    "Compute to Suboptimal")) {
                    Planner.Compute(Status.PathStatus.Suboptimal, MaxSteps, MaxTimeMS);
                    updatestatus = true;
                }
                if (GUI.Button(new Rect(Screen.width - 150, 80, 150, 20),
                    "Compute to Partial")) {
                    Planner.Compute(Status.PathStatus.Partial, MaxSteps, MaxTimeMS);
                    updatestatus = true;
                }
                if (GUI.Button(new Rect(Screen.width - 150, 100, 150, 20),
                    "ComputeAsync BROKEN")) {
                    Planner.ComputeAsync(Status.PathStatus.RunForever, -1, MaxTimeMS);
                    updatestatus = true;
                }
                if (GUI.Button(new Rect(Screen.width - 150, 120, 150, 20),
                    "Step")) {
                    Planner.Step();
                    updatestatus = true;
                }
                if (GUI.Button(new Rect(Screen.width - 150, 140, 150, 20),
                    "Reset")) {
                    Planner.Reset();
                    updatestatus = true;
                }
                if (GUI.Button(new Rect(Screen.width - 150, 160, 150, 20),
                    "Start modified")) {
                    S1 = Dom.StateForPosition(StartPos.position.FromUnity());
                    Planner.Start = S1;
                    updatestatus = true;
                }
                if (updatestatus) {
                    statusText = string.Format("{0}, visited={1}, w={2}", 
                        Planner.Status,
                        Planner.NodesVisited,
                        Planner.Reached == null ? "N/A" :
                        Planner.Weighting.CostMultiplier(Planner.Reached.Position).ToString());
                    Debug.Log(statusText);
                }
            }
        }
    }
}
