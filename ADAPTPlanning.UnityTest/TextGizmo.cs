/*
*   TextGizmo.cs
*   August 2009
*   Carl Emil Carlsen
*   sixthsensor.dk
*/
using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Provides text gizmos for numeric debugging.
/// </summary>
public class TextGizmo
{
    static TextGizmo tg;
    readonly Dictionary<char, string> texturePathLookup;
    readonly Camera editorCamera;
    const int CharTexHeight = 10;
    // todo: line breaks
    const int CharTexWidth = 6;
    const string Chr = "abcdefghijklmnopqrstuvwxyz0123456789 !#%'()+,-.;=_{}~";

    /// <summary>
    /// Initialize the singleton.
    /// </summary>
    public static void Init()
    {
        tg = new TextGizmo();
    }

    /// <summary>
    /// Singleton constructor.
    /// </summary>
    TextGizmo()
    {
        editorCamera = Camera.current;
        texturePathLookup = new Dictionary<char, string>();
        for (int c = 0; c < Chr.Length; c++) {
            texturePathLookup.Add(Chr[c], "TextGizmo/text_" + Chr[c] + ".png");
        }
        texturePathLookup.Add('*', "TextGizmo/text_star.png");
        texturePathLookup.Add('/', "TextGizmo/text_slash.png");
        texturePathLookup.Add('\\', "TextGizmo/text_backslash.png");
        texturePathLookup.Add('"', "TextGizmo/text_quotes.png");
        texturePathLookup.Add('?', "TextGizmo/text_questionmark.png");
        texturePathLookup.Add(':', "TextGizmo/text_colon.png");
    }

    /// <summary>
    /// Draw the text gizmos. To be called from OnGizmos.
    /// </summary>
    public static void Draw(Vector3 position, string text)
    {
        if (tg == null) {
            Init();
        }

        string lowerText = text.ToLower();
        Vector3 screenPoint = tg.editorCamera.WorldToScreenPoint(position);
        int xoffset = 0;
        int yoffset = 0;
        for (int c = 0; c < lowerText.Length; c++) {
            if (lowerText[c] == '\n') {
                xoffset = 0;
                yoffset -= CharTexHeight;
            } else if (tg.texturePathLookup.ContainsKey(lowerText[c])) {
                Vector3 worldPoint = tg.editorCamera.ScreenToWorldPoint(
                    new Vector3(
                    screenPoint.x + xoffset,
                    screenPoint.y + yoffset,
                    screenPoint.z));
                Gizmos.DrawIcon(worldPoint,
                                tg.texturePathLookup[lowerText[c]], false);
                xoffset += CharTexWidth;
            }
        }
    }
}
