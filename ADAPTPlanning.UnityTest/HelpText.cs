namespace ADAPTPlanning.UnityTest
{
    using UnityEngine;

    /// <summary>
    /// Provides a help text (e.g. for a legend or instructions).
    /// </summary>
    public class HelpText : MonoBehaviour
    {
        /// <summary>
        /// The help text to draw on the screen.
        /// </summary>
        public string[] Text;

        void OnGUI()
        {
            var style = new GUIStyle();
            style.normal.textColor = Color.white;
            foreach (var s in Text) {
                GUILayout.Label(s, style);
            }
        }
    }
}
