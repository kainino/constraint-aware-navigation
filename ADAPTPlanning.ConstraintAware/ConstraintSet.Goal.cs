namespace ADAPTPlanning.ConstraintAware
{
    partial class ConstraintSet
    {
        /// <summary>
        /// A plan goal: currently, just an annotation.
        /// </summary>
        [System.Serializable]
        public class Goal
        {
            /// <summary>
            /// Preposition relative to annotation (currently just "[Move] To").
            /// </summary>
            public GoalPrep Preposition;

            /// <summary>
            /// Annotation used to define goal location.
            /// </summary>
            public Annotation Annotation;

            /// <inheritdoc/>
            public override string ToString()
            {
                return string.Format("({0} {1})", Preposition, Annotation);
            }
        }
    }
}