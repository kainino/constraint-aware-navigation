namespace ADAPTPlanning.ConstraintAware
{
    using System;
    using UE = UnityEngine;

    /// <summary>
    /// An annotation representing a humanoid sensory area. Based on Nathan
    /// Sturtevant's "Incorporating Human Relationships Into Path Planning".
    /// </summary>
    [System.Serializable]
    public class HumanAnnotation
        : Annotation
    {
        /// <summary>
        /// The Unity object representing the human.
        /// </summary>
        public UE.Transform Obj;

        /// <summary>
        /// The "personal space" radius (nearby perception).
        /// </summary>
        public float PersonalRadius;

        /// <summary>
        /// The field of view angle on the vision cone.
        /// </summary>
        public float SightAngle;

        /// <summary>
        /// The line of sight maximum distance. In practice, dependent on
        /// environmental conditions.
        /// </summary>
        public float SightDistance;

        /// <inheritdoc/>
        public override Vector3 Position {
            get { return Obj.transform.position.FromUnity(); }
        }

        /// <inheritdoc/>
        public override bool IsStatic {
            get { return Obj.gameObject.isStatic; }
        }

        /// <inheritdoc/>
        public override float Contains(Vector3 pos, float radius)
        {
            var direction = Obj.position - pos.ToUnity();
            float dist = direction.magnitude;
            float personal = UE.Mathf.Max(0, 1 - dist / PersonalRadius);
            direction.Normalize();
            float cosAng = UE.Vector3.Dot(direction, -Obj.forward);
            float cosSightAng = UE.Mathf.Cos(SightAngle / 180 * UE.Mathf.PI);
            float sightdist = UE.Mathf.Max(0, 1 - dist / SightDistance);
            float sight = cosAng > cosSightAng ? sightdist : 0;
            return Math.Min(1, sight + personal);
        }
    }
}
