namespace ADAPTPlanning.ConstraintAware
{
    using ADAPTPlanning;

    /// <summary>
    /// An Annotation, which provides spatial information to constraint
    /// definitions.
    /// </summary>
    public abstract class Annotation
        : UnityEngine.MonoBehaviour
    {
        /// <summary>
        /// Position of the annotation (usually the center).
        /// </summary>
        public abstract Vector3 Position { get; }

        /// <summary>
        /// Whether the annotation is static (never moves during planning).
        /// </summary>
        public abstract bool IsStatic { get; }

        /// <summary>
        /// 1 if the given position is within the given radius of the annotation
        /// volume, 0 otherwise.
        /// </summary>
        public abstract float Contains(Vector3 pos, float radius);
    }

    /// <summary>
    /// A fuzzy annotation, which has fuzzy edges.
    /// </summary>
    public abstract class AnnotationFuzzy
        : Annotation
    {
        /// <summary>
        /// Finds the distance from the position to the nearest point on the
        /// annotation.
        /// </summary>
        /// <returns>The distance.</returns>
        /// <param name="pos">Position.</param>
        public abstract float MinDistance(Vector3 pos);

        //public abstract Vector3 ClosestPoint(Vector3 pos);
    }
}
