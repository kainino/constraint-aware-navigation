namespace ADAPTPlanning.ConstraintAware
{
    using System;
    using System.Linq;

    /// <summary>
    /// A set of constraints which can be used to constrain path planning.
    /// </summary>
    [Serializable]
    public partial class ConstraintSet
        : Weighting
    {
        float maxWeight;
        Constraint[] prevConstrs = null;

        /// <summary>
        /// The constraints in the set.
        /// </summary>
        public Constraint[] Constrs;

        /// <inheritdoc/>
        public override string ToString()
        {
            var cs = Constrs.Select(c => c.ToString()).ToArray();
            var css = "(" + string.Join(", ", cs) + ")";

            return string.Format("[ConstraintSet: {0}]", css);
        }

        /// <inheritdoc/>
        public override float WeightBase()
        {
            if (Constrs != prevConstrs) {
                prevConstrs = Constrs;
                if (Constrs.Length == 0) {
                    maxWeight = 0;
                } else {
                    var maxconstr = Constrs.MinBy(c => -c.Weight);
                    var wt = Math.Max(0, maxconstr.Weight);
                    maxWeight = maxconstr != null ? wt : 0;
                }
            }
            return maxWeight;
        }

        /// <inheritdoc/>
        public override float WeightField(Vector3 mp)
        {
            float wt = 0;
            if (Constrs != null) {
                foreach (var c in Constrs) {
                    wt += c.WeightField(mp);
                }
            }
            return wt;
        }
    }
}
