namespace ADAPTPlanning.ConstraintAware
{
    /// <summary>
    /// An annotation provided by a Unity BoxCollider.
    /// </summary>
    [System.Serializable]
    public class BoxAnnotation
        : AnnotationFuzzy
    {
        /// <summary>
        /// The BoxCollider which defines the annotation volume.
        /// </summary>
        public UnityEngine.BoxCollider Box;

        /// <inheritdoc/>
        public override Vector3 Position {
            get { return transform.position.FromUnity(); }
        }

        /// <inheritdoc/>
        public override bool IsStatic {
            get { return gameObject.isStatic; }
        }

        /// <inheritdoc/>
        public override float Contains(Vector3 pos, float radius)
        {
            return MinDistance(pos) < radius ? 1 : 0;
        }

        /// <inheritdoc/>
        public override float MinDistance(Vector3 pos)
        {
            return Box.MinDistance(pos);
        }
    }
}
