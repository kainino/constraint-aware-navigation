namespace ADAPTPlanning.ConstraintAware
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ADAPTPlanning;

    partial class ConstraintSet
    {
        /// <summary>
        /// A single constraint: [In|Near] [annotation] with [weight]
        /// </summary>
        [Serializable]
        public class Constraint
        {
            /// <summary>
            /// Preposition (In|Near) relative to annotation.
            /// </summary>
            public ConstrPrep Preposition;

            /// <summary>
            /// Annotation used to define constraint.
            /// </summary>
            public Annotation Annotation;

            /// <summary>
            /// Weight of this constraint.
            /// </summary>
            public float Weight;

            /// <summary>
            /// The exact radius of influence.
            /// </summary>
            public float InfluenceRadius {
                get {
                    if (Preposition == ConstrPrep.In) {
                        return 0.0001f;
                    }
                    return Math.Abs(Weight);
                }
            }

            /// <inheritdoc/>
            public override string ToString()
            {
                return string.Format("({0} {1} w={2})",
                    Preposition, Annotation, Weight);
            }

            //Dictionary<DefaultState, float> lastupdate;
            List<State> lastupdate;
            State laststate;

            /// <summary>
            /// Given a planner, determine which state nodes need to be updated/recalculated.
            /// </summary>
            IEnumerable<State> FindNodesToUpdate(Planner<Domain<State>, State> planner)
            {
                var state = planner.Domain.StateForPosition(Annotation.Position);
                IEnumerable<State> changed;
                // apparently isStatic is an editor-only property? -KN
                if (Annotation.IsStatic || state == laststate) {
                    return null;
                }
                laststate = state;
            
                var r = InfluenceRadius;

                var influenced = planner.Visited
                    .Select(t => t.To)
                    .Where(n => Annotation.Contains(n.Position, r) > 0.999)
                    .ToList();
                changed = lastupdate == null ? influenced :
                    influenced.Union(lastupdate);
                lastupdate = influenced;
                return changed;
            }

            /// <summary>
            /// Computes the weight field at a given point in space.
            /// </summary>
            public float WeightField(Vector3 mp)
            {
                if (Annotation == null) {
                    return 0f;
                }

                var ann = Annotation as AnnotationFuzzy;
                if (ann != null) {
                    var r = ann.MinDistance(mp);
                    float falloff = (InfluenceRadius - r) / InfluenceRadius;
                    return Weight * Math.Max(0, falloff);
                }
                return Annotation.Contains(mp, InfluenceRadius) * Weight;
            }
        }
    }
}