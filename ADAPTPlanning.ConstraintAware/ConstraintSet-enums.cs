namespace ADAPTPlanning.ConstraintAware
{
    partial class ConstraintSet
    {
        /// <summary> A tactical verb/action. </summary>
        public enum Verb
        {
            /// <summary> Move (to) a position. </summary>
            Move,
            /// <summary> Look at something. </summary>
            LookAt,
        }

        /// <summary>
        /// Adverbs which describe behavior modes. Not used currently.
        /// </summary>
        public enum Adverb
        {
            /// <summary> No particular adverb. </summary>
            None,
            /// <summary> Do so quickly. </summary>
            Quickly,
            /// <summary> Do so slowly. </summary>
            Slowly,
            /// <summary> Do so covertly. </summary>
            Covertly,
        }

        /// <summary> Constraint prepositions. </summary>
        public enum ConstrPrep
        {
            /// <summary> Inactive constraint. </summary>
            None,
            /// <summary> The space inside the constraint. </summary>
            In,
            /// <summary> The space near the constraint. </summary>
            Near,
        }

        /// <summary> Goal prepositions. </summary>
        public enum GoalPrep
        {
            /// <summary> Move "to" the goal. </summary>
            To,
        }
    }
}