namespace ADAPTPlanning
{
    /// <summary>
    /// Extensions to convert between Unity and ADAPTPlanning Vector3 types.
    /// </summary>
    public static class UnityExtensions
    {
        /// <summary>
        /// Convert an <see cref="ADAPTPlanning.Vector3"/> to a
        /// <see cref="UnityEngine.Vector3"/>.
        /// </summary>
        public static UnityEngine.Vector3 ToUnity(this Vector3 v)
        {
            return new UnityEngine.Vector3(v.X, v.Y, v.Z);
        }

        /// <summary>
        /// Convert a <see cref="UnityEngine.Vector3"/> to an
        /// <see cref="ADAPTPlanning.Vector3"/>.
        /// </summary>
        public static Vector3 FromUnity(this UnityEngine.Vector3 v)
        {
            return new Vector3(v.x, v.y, v.z);
        }
    }
}
