namespace ADAPTPlanning
{
    using UnityEngine;

    /// <summary>
    /// Extensions for computations involving <see cref="BoxCollider"/>s.
    /// </summary>
    public static class ExtensionsBoxCollider
    {
        /// <summary>
        /// Calculates whether a <see cref="BoxCollider"/> contains a
        /// <see cref="Vector3"/> point.
        /// </summary>
        public static bool Contains(this BoxCollider bc, Vector3 p)
        {
            var t = bc.transform;
            var p2 = t.InverseTransformPoint(p.ToUnity());
            float
                xmin = bc.center.x - bc.size.x / 2 - 0.000001f,
                xmax = bc.center.x + bc.size.x / 2 + 0.000001f,
                ymin = bc.center.y - bc.size.y / 2 - 0.000001f,
                ymax = bc.center.y + bc.size.y / 2 + 0.000001f,
                zmin = bc.center.z - bc.size.z / 2 - 0.000001f,
                zmax = bc.center.z + bc.size.z / 2 + 0.000001f;

            return
                xmin < p2.x && p2.x < xmax &&
                ymin < p2.y && p2.y < ymax &&
                zmin < p2.z && p2.z < zmax;
        }

        /// <summary>
        /// Calculates the minimum distance between a <see cref="Vector3"/>
        /// point and a <see cref="BoxCollider"/>.
        /// </summary>
        public static float MinDistance(this BoxCollider bc, Vector3 p)
        {
            return Vector3.Distance(bc.ClosestPoint(p), p);
        }

        /// <summary>
        /// Finds the closest point on a <see cref="BoxCollider"/> to a
        /// <see cref="Vector3"/> point.
        /// </summary>
        public static Vector3 ClosestPoint(this BoxCollider bc, Vector3 p)
        {
            var t = bc.transform;
            var p2 = t.InverseTransformPoint(p.ToUnity());
            float
                xmin = bc.center.x - bc.size.x / 2,
                xmax = bc.center.x + bc.size.x / 2,
                ymin = bc.center.y - bc.size.y / 2,
                ymax = bc.center.y + bc.size.y / 2,
                zmin = bc.center.z - bc.size.z / 2,
                zmax = bc.center.z + bc.size.z / 2;

            p2.x = Mathf.Clamp(p2.x, xmin, xmax);
            p2.y = Mathf.Clamp(p2.y, ymin, ymax);
            p2.z = Mathf.Clamp(p2.z, zmin, zmax);

            return t.TransformPoint(p2).FromUnity();
        }
    }
}
